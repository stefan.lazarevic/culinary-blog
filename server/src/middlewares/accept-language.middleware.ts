import type { Request, Response, NextFunction } from "express";
import ResponseError from "../lib/response.error";

export default function createAcceptLanguageMiddleware(
	supportedLanguages: string[] | Readonly<string[]>
) {
	return function middleware(
		request: Request,
		response: Response,
		next: NextFunction
	) {
		let acceptedLanguage = request.acceptsLanguages(
			supportedLanguages as string[]
		);
		if (!acceptedLanguage) {
			const error = ResponseError.create(406);
			error.detail = "Requested language is not supported.";
			error.headers = "Accept-Language";
			return next(error);
		}

		request.headers["accept-language"] = acceptedLanguage;
		next();
	};
}
