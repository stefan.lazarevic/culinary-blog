import type { Request, Response, NextFunction } from "express";
import { validationResult } from "express-validator";
import ResponseErrors from "../lib/response-errors";
import ResponseError from "../lib/response.error";

const customValidationResult = validationResult.withDefaults({
	formatter: (error) => {
		const customError = ResponseError.create(400);
		customError.detail = error.msg;
		if (error.location) {
			customError[error.location] = error.param;
		}
		return customError;
	},
});

export default function collectValidationErrors() {
	return function middleware(
		request: Request,
		response: Response,
		next: NextFunction
	) {
		const result = customValidationResult(request);

		if (!result.isEmpty()) {
			const errors = new ResponseErrors();
			errors.status = 400;
			result.array().forEach((error) => {
				errors.push(error);
			});
			return next(errors);
		}

		next();
	};
}
