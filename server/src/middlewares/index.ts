import type { Express, NextFunction, Request, Response } from "express";
import express from "express";
import helmet from "helmet";
import { SUPPORTED_LANGUAGES } from "../config/languages.config";
import Logger from "../lib/logger";
import ResponseError from "../lib/response.error";
import createAcceptLanguageMiddleware from "./accept-language.middleware";
import createUnhandledErrorMiddleware from "./error.middleware";

export default function bootstrapIncomingMiddlewares(app: Express) {
	app
		.use(helmet())
		.use(express.json())
		.use(createAcceptLanguageMiddleware(SUPPORTED_LANGUAGES));
}

export function bootstrapOutgoingMiddlewares(app: Express) {
	app.use("*", (request: Request, response: Response, next: NextFunction) => {
		next(ResponseError.create(404));
	});
	app.use(createUnhandledErrorMiddleware({ logger: Logger.getInstance() }));
}
