import type { Request, Response, NextFunction } from "express";
import ResponseError from "../lib/response.error";
import IAsyncCache from "../typescript/interfaces/async-cache.interface";

interface IRateLimitMiddlewareOptions {
	/**
	 * Maximum number of reqeusts in one time window.
	 */
	max: number;
	/**
	 * Time expressed in miliseconds when the limit expires.
	 */
	expiresAt: number;
}

export interface IRequestRecord {
	/**
	 *
	 */
	count: number;
	/**
	 *
	 */
	timestamp: number;
}

/**
 *
 * @param cache
 * @param options
 * @returns
 */
export default function createRateLimitMiddleware(
	cache: IAsyncCache<IRequestRecord>,
	options: Partial<IRateLimitMiddlewareOptions> = {}
) {
	const _options: IRateLimitMiddlewareOptions = {
		max: 5,
		expiresAt: 60 * 60 * 1000,
		...options,
	};

	return function middleware(
		request: Request,
		response: Response,
		next: NextFunction
	) {
		cache.get(request.ip).then(async (record) => {
			if (!record || Date.now() > record!.timestamp + _options.expiresAt) {
				record = {
					count: 1,
					timestamp: new Date().getTime(),
				};
				response.setHeader("X-Rate-Limit-Limit", record!.count);
				response.setHeader(
					"X-Rate-Limit-Remaining",
					_options.max - record!.count
				);
				response.setHeader("X-Rate-Limit-Reset", record!.timestamp);
				await cache.set(request.ip, record!);
				return next();
			}

			++record!.count;
			cache.set(request.ip, record!).then(async () => {
				if (record!.count > _options.max) {
					const error = ResponseError.create(429);
					error.detail =
						"The server cannot process this request right now. Please try again later.";
					return next(error);
				}

				response.setHeader("X-Rate-Limit-Limit", record!.count);
				response.setHeader(
					"X-Rate-Limit-Remaining",
					_options.max - record!.count
				);
				response.setHeader("X-Rate-Limit-Reset", record!.timestamp);
				next();
			});
		});
	};
}
