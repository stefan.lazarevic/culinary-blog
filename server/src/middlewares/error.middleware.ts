import type { Request, Response, NextFunction } from "express";
import ResponseErrors from "../lib/response-errors";
import ResponseError from "../lib/response.error";
import ILogger from "../typescript/interfaces/logger.interface";

interface IUnhandledErrorMiddlewareOptions {
	logger?: ILogger;
}

export default function createUnhandledErrorMiddleware({
	logger,
}: IUnhandledErrorMiddlewareOptions = {}) {
	return function unhandledErrorMiddleware(
		error: ResponseError | Error,
		request: Request,
		response: Response,
		next: NextFunction
	) {
		if (error instanceof ResponseError || error instanceof ResponseErrors) {
			return response.status(error.status).json(error);
		}

		if (error instanceof Error) {
			const responseError = new ResponseError();
			responseError.detail = error.message;
			if (logger) logger.error(error.message);
			return response.status(responseError.status).json(responseError);
		}

		next();
	};
}
