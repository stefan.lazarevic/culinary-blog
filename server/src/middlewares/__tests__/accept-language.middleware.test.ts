import type { Request, Response } from "express";
import createAcceptLanguageMiddleware from "../accept-language.middleware";

describe("Accept-Language middleware", () => {
	it("should reject request with 406 status when requested language is not supported.", () => {
		const mockedNext = jest.fn();
		const middleware = createAcceptLanguageMiddleware(["en"]);
		const request: Request = {
			acceptsLanguages: jest.fn().mockReturnValue(false),
			headers: {},
		} as Partial<Request> as Request;
		const response: Response = {} as Response;
		middleware(request, response, mockedNext);
		expect(mockedNext).toBeCalledWith(
			expect.objectContaining({
				status: 406,
				source: {
					header: "Accept-Language",
				},
			})
		);
	});

	it("should pass request when language is supported.", () => {
		const mockedNext = jest.fn();
		const middleware = createAcceptLanguageMiddleware(["en", "es"]);
		const request: Request = {
			acceptsLanguages: jest.fn().mockReturnValue(true),
			headers: {},
		} as Partial<Request> as Request;
		const response = {} as Response;
		middleware(request, response, mockedNext);
		expect(mockedNext).not.toBeCalledWith(
			expect.objectContaining({
				status: 406,
				source: {
					header: "Accept-Language",
				},
			})
		);
	});

	it("should overwrite accept-languages header with the first matching language.", () => {
		const mockedNext = jest.fn();
		const middleware = createAcceptLanguageMiddleware(["en", "es"]);
		const request: Request = {
			acceptsLanguages: jest.fn().mockReturnValue("es"),
			headers: {},
		} as Partial<Request> as Request;
		const response = {} as Response;
		middleware(request, response, mockedNext);
		expect(request.headers).toHaveProperty("accept-language", "es");
	});
});
