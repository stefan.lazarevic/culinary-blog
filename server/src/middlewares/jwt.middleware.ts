import type { Request, Response, NextFunction } from "express";
import jwt, { VerifyOptions } from "jsonwebtoken";
import ResponseError from "../lib/response.error";
import getJWTSecretKey from "../utils/get-jwt-secret-key.util";

export default function createJWTMiddleware(options: VerifyOptions) {
	return function middleware(
		request: Request,
		response: Response,
		next: NextFunction
	) {
		const header = request.header("Authorization");
		const token =
			header && header.indexOf("Bearer") > -1 ? header.split(" ")[1] : "";
		if (!token) {
			const error = ResponseError.create(400);
			error.detail = "No authorization found.";
			error.headers = "Authorization";
			return next(error);
		}
		const output = jwt.verify(token, getJWTSecretKey(), options);
		if (!output) {
			const error = ResponseError.create(401);
			error.detail =
				"You have attempted to access a resource for which you are not authorized.";
			return next(error);
		}
		next();
	};
}
