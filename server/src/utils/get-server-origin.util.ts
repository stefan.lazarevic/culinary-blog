import getServerDomain from "./get-server-domain.util";
import getServerScheme from "./get-server-scheme.util";

export default function getServerOrigin(): string {
	const scheme = getServerScheme();
	const domain = getServerDomain();
	return `${scheme}://${domain}`;
}
