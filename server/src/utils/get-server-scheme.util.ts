export default function getServerScheme() {
	return process.env.SERVER_SCHEME || "http";
}
