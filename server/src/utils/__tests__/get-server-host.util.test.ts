import getServerHost from "../get-server-host.util";

describe("getServerHost()", () => {
	it("should match localhost domain as a default.", () => {
		expect(getServerHost()).toBe("localhost");
	});
	it("should use configured process host.", () => {
		const domain = process.env.SERVER_DOMAIN;
		const port = process.env.SERVER_PORT;
		process.env.SERVER_DOMAIN = "dev.stefanlazarevic";
		process.env.SERVER_PORT = "3000";
		expect(getServerHost()).toBe(
			process.env.SERVER_DOMAIN + ":" + process.env.SERVER_PORT
		);
		process.env.SERVER_DOMAIN = domain;
		process.env.SERVER_PORT = port;
	});
});
