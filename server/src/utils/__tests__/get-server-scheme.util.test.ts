import getServerScheme from "../get-server-scheme.util";

describe("getServerScheme()", () => {
	it("should use port http as a default.", () => {
		expect(getServerScheme()).toBe("http");
	});
	it("should use configured process scheme.", () => {
		const current = process.env.SERVER_SCHEME;
		process.env.SERVER_SCHEME = "https";
		expect(getServerScheme()).toBe(process.env.SERVER_SCHEME);
		process.env.SERVER_PORT = current;
	});
});
