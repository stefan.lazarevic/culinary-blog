import pluck from "../pluck.util";

describe("pluck()", () => {
	it("should return default value when not found.", () => {
		expect(pluck("a", {}, "default")).toBe("default");
	});
	it("should return value stored under the asked property.", () => {
		expect(pluck("a", { a: 10 })).toBe(10);
	});
	it("should be able to pick value of a nested property.", () => {
		expect(pluck("a.b", { a: { b: 15 } })).toBe(15);
	});
});
