import getServerPort from "../get-server-port.util";

describe("getServerPort()", () => {
	it("should use port 80 as a default.", () => {
		expect(getServerPort()).toBe("80");
	});
	it("should use configured process port.", () => {
		const current = process.env.SERVER_PORT;
		process.env.SERVER_PORT = "100";
		expect(getServerPort()).toBe(process.env.SERVER_PORT);
		process.env.SERVER_PORT = current;
	});
});
