import getServerDomain from "../get-server-domain.util";

describe("getServerDomain()", () => {
	it("should use localhost domain as a default.", () => {
		expect(getServerDomain()).toBe("localhost");
	});
	it("should use configured process domain.", () => {
		const current = process.env.SERVER_DOMAIN;
		process.env.SERVER_DOMAIN = "dev.stefanlazarevic";
		expect(getServerDomain()).toBe(process.env.SERVER_DOMAIN);
		process.env.SERVER_DOMAIN = current;
	});
});
