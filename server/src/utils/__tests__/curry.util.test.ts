import { curry } from "../curry.util";

describe("curry()", () => {
	it("breaks down function arguments into multiple functions.", () => {
		const add = (x: number, y: number, z: number) => x + y + z;
		const result = curry(add)(1)(2)(3);
		expect(result).toBe(6);
	});
});
