export default function getServerDomain() {
	return process.env.SERVER_DOMAIN || "localhost";
}
