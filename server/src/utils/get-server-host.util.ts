import getServerDomain from "./get-server-domain.util";
import getServerPort from "./get-server-port.util";

export default function getServerHost() {
	const domain = getServerDomain();
	const port = getServerPort();
	return `${domain}${port === "80" ? "" : `:${port}`}`;
}
