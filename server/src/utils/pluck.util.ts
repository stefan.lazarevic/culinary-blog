/**
 * @description
 * Extract specified property from the underlying object.
 * @param {string} property CSV property path.
 * @param {object} object
 * @param {*} defaultValue
 * @returns {*}
 * @example
 * pluck("a.b.c", {a: {b: c: 10}}), 20); // 10
 * // Same as in ES2021
 * ({a: {b: c: 10}}).a?.b?.c || 20;
 */
export default function pluck(property: string | string[], object: object, defaultValue: any = null) {
	if (typeof property !== "string" && !Array.isArray(property)) {
		 return defaultValue;
	}
	const properties = Array.isArray(property) ? property : property.split(".");
	const output = properties.reduce(function pluckExisting(nestedObject: Record<string, any>, property: string) {
		 if (nestedObject && typeof nestedObject.hasOwnProperty === "function" && nestedObject.hasOwnProperty(property)) {
				return nestedObject[property];
		 }
		 return null;
	}, object);
	return output !== null ? output : defaultValue;
}
