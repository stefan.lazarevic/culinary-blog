export default function getServerPort(): string {
	return process.env.SERVER_PORT || "80";
}
