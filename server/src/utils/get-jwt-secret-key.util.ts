export default function getJWTSecretKey(): string {
	const key = process.env.JWT_SECRET_KEY;
	if(!key) {
		throw new Error("JWT_SECRET_KEY is not configured in the environment.");
	}
	return key;
}
