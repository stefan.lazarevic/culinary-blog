export default function getSendGridApiKey() {
	const key = process.env.SENDGRID_API_KEY;
	if (!key) {
		throw new Error("Send Grid API key is not configured.");
	}
	return key;
}
