export const SUPPORTED_LANGUAGES = ["en", "fr", "it"] as const;
export type SUPPORTED_LANGUAGES = typeof SUPPORTED_LANGUAGES[number];
