import type { Request, Response, NextFunction } from "express";
import { v4 as uuidv4 } from "uuid";
import jwt from "jsonwebtoken";
import EmailService from "../services/email.service";
import Email from "../typescript/@types/email.type";
import getServerOrigin from "../utils/get-server-origin.util";
import getJWTSecretKey from "../utils/get-jwt-secret-key.util";

export function authenticateHandler(
	request: Request,
	response: Response,
	next: NextFunction
) {
	// ✔ Prevent brute force access.
	// ✔ Validate incoming request data.
	// Check for the user existence.
	// ✔ Construct a magic link.
	// ✔ Send email to the user.
	const uuid = uuidv4();
	const verifyLink = `${getServerOrigin()}/verify?email=${
		request.body.email
	}&uuid=${uuid}`;
	const email: Email = {
		from: process.env.EMAIL_SENDER!,
		to: request.body.email,
		subject: "Culinary Blog - Magic Link",
		html: `
			<h3><a href="${verifyLink}">${verifyLink}</a></h3>
		`,
	};
	const service = new EmailService();
	service
		.send(email)
		.then(() => {
			response.sendStatus(202);
		})
		.catch((error) => {
			next(error);
		});
}

export function verificationHandler(
	request: Request,
	response: Response,
	next: NextFunction
) {
	// extract email and uuid from the request params.
	// get user where email and uuid matches in the database.
	const user = { email: "stefanlazarevic.contact@gmail.com" };
	// generate token
	const token = jwt.sign(user, getJWTSecretKey(), {
		algorithm: "HS256",
		issuer: getServerOrigin(),
	});
	response.status(200).json({
		token,
	});
}
