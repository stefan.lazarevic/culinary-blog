type Envelope = {
	from: string;
	to: string[];
	subject: string;
}

type Content = { html: string } | { text: string };

type Email = Envelope & Content;

export default Email;
