export type RequestErrorSource = "body" | "headers" | "parameters" | "query" | "cookies";
