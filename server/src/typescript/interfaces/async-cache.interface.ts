export default interface IAsyncCache<T> {
	set(key: string, value: T): Promise<void>;
	get(key: string): Promise<T | undefined>;
	has(key: string): Promise<boolean>;
	delete(key: string): Promise<void>;
}
