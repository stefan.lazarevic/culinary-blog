import type Email from "../@types/email.type";

export default interface IEmailStrategy {
	send(email: Email): Promise<any>;
}
