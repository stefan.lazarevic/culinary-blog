/**
 * @openapi
 * components:
 *   schemas:
 *     PaginationLinks:
 *       type: object
 *       properties:
 *         first:
 *           type: string
 *           format: uri
 *         last:
 *           type: string
 *           format: uri
 *         prev:
 *           type: string
 *           format: uri
 *         next:
 *           type: string
 *           format: uri
 */
export default interface IPaginationLinks {
	first: string;
	last: string;
	prev: string;
	next: string;
}
