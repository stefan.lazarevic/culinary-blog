export default interface ResponseError extends Error {
  status: number;
	title: string;
	detail: string;
}
