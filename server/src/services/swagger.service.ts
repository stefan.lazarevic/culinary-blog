import type { Express } from "express";
import swaggerJSDoc, { Options } from "swagger-jsdoc";
import swaggerUiExpress from "swagger-ui-express";
import path from "path";
import getServerHost from "../utils/get-server-host.util";

export default function bootstrapSwagger(app: Express) {
	const OPTIONS: Options = {
		definition: {
			openapi: "3.0.3",
			info: {
				title: "REST API Documentation",
				version: "1.0",
				contact: {
					name: "Stefan Lazarevic",
					email: "stefan.lazarevic@quantox.com",
				},
				license: {
					name: "MIT",
					url: "https://mit-license.org/",
				},
			},
			explorer: true,
			host: getServerHost(),
		},
		apis: [path.resolve(__dirname, "../**/*.ts")],
	};

	app.use(
		"/docs",
		swaggerUiExpress.serve,
		swaggerUiExpress.setup(swaggerJSDoc(OPTIONS))
	);
}
