import SendGridEmailStrategy from "../lib/sendgrid-strategy";
import IEmailStrategy from "../typescript/interfaces/email-strategy.interface";
import type Email from "../typescript/@types/email.type";

export default class EmailService {
	#strategy: IEmailStrategy;

	constructor() {
		this.#strategy = new SendGridEmailStrategy();
	}

	use(strategy: IEmailStrategy): this {
		this.#strategy = strategy;
		return this;
	}

	send(email: Email) {
		return this.#strategy.send(email);
	}
}
