import ILogger from "../typescript/interfaces/logger.interface";
import winston from "winston";

const logger = winston.createLogger({
	level: "info",
	transports: [
		new winston.transports.Console()
	],
});

let instance: Logger;

/**
 * Facade class that abstracts underlying logging implementation.
 */
export default class Logger implements ILogger {
	static getInstance() {
		return new Logger();
	}
	constructor() {
		if (instance) {
			return instance;
		}
		instance = this;
	}
	debug(message: string): void {
		logger.debug(message);
	}
	info(message: string): void {
		logger.info(message);
	}
	warn(message: string): void {
		logger.warn(message);
	}
	error(message: string): void {
		logger.error(message);
	}
	fatal(message: string): void {
		logger.crit(message);
	}
}
