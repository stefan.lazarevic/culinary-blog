import ResponseError from "./response.error";

export default class ResponseErrors extends Error {
	#status: number;
	#errors: ResponseError[];

	constructor() {
		super();
		this.#status = 500;
		this.#errors = [];
	}

	get length() {
		return this.#errors.length;
	}

	get status() {
		return this.#status;
	}

	set status(status: number) {
		this.#status = status;
	}

	push(error: ResponseError) {
		this.#errors.push(error);
	}

	pop() {
		return this.#errors.pop();
	}

	toJSON() {
		return {
			status: this.#status,
			errors: this.#errors
		}
	}
}
