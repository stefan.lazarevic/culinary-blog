import { STATUS_CODES } from "http";

/**
 * @openapi
 * components:
 *   schemas:
 *     ResponseError:
 *       type: object
 *       properties:
 *         status:
 *           type: number
 *         title:
 *           type: string
 *         detail:
 *           type: string
 *         source:
 *           type: object
 *           properties:
 *             headers:
 *               type: string
 *             body:
 *               type: string
 *             params:
 *               type: string
 *             query:
 *               type: string
 *             cookies:
 *               type: string
 */
export default class ResponseError extends Error {
	#status: number;
	#title: string | undefined;
	detail: string;
	source: {
		headers?: string;
		body?: string;
		params?: string;
		query?: string;
		cookies?: string;
	};

	constructor() {
		super();
		this.#status = 500;
		this.#title = STATUS_CODES[this.#status];
		this.detail = "";
		this.source = {};
	}

	static create(status: number) {
		const error = new ResponseError();
		error.status = status;
		return error;
	}

	set status(status: number) {
		this.#status = status;
		this.#title = STATUS_CODES[this.#status];
	}

	get status() {
		return this.#status;
	}

	get title() {
		return this.#title;
	}

	set body(body: string) {
		this.source.body = body;
	}

	get body() {
		return this.source.body || "";
	}

	set params(params: string) {
		this.source.params = params;
	}

	get params() {
		return this.source.params || "";
	}

	set headers(headers: string) {
		this.source.headers = headers;
	}

	get headers() {
		return this.source.headers || "";
	}

	set query(query: string) {
		this.source.query = query;
	}

	get query() {
		return this.source.query || "";
	}

	set cookies(cookies: string) {
		this.source.cookies = cookies;
	}

	get cookies() {
		return this.source.cookies || "";
	}

	toJSON() {
		return {
			status: this.status,
			title: this.title,
			detail: this.detail,
			source: this.source,
		};
	}
}
