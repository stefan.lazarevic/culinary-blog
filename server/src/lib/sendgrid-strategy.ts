import mailer from "@sendgrid/mail";
import IEmailStrategy from "../typescript/interfaces/email-strategy.interface";
import type Email from "../typescript/@types/email.type";
import getSendGridApiKey from "../utils/get-sendgrid-api-key.util";

export default class SendGridEmailStrategy implements IEmailStrategy {
	constructor() {
		mailer.setApiKey(getSendGridApiKey());
	}

	send(email: Email) {
		return mailer.send(email);
	}
}
