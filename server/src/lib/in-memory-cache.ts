import IAsyncCache from "../typescript/interfaces/async-cache.interface";

export default class InMemoryCache<T> implements IAsyncCache<T> {
	storage: Map<string, T>;

	constructor() {
		this.storage = new Map<string, T>();
	}
	async set(key: string, value: T): Promise<void> {
		this.storage.set(key, value);
	}
	async get(key: string): Promise<T | undefined> {
		return this.storage.get(key);
	}
	async has(key: string): Promise<boolean> {
		return this.storage.has(key);
	}
	async delete(key: string): Promise<void> {
		this.storage.delete(key);
	}
}
