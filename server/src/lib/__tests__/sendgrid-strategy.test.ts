import sendGridService from "@sendgrid/mail";
import Email from "../../typescript/@types/email.type";
import SendGridEmailStrategy from "../sendgrid-strategy";

jest.mock("@sendgrid/mail", () => ({
	setApiKey: jest.fn(),
	send: jest.fn(),
}));

jest.mock("../../utils/get-sendgrid-api-key.util", () => jest.fn());

describe("SendGridEmailStrategy", () => {
	it("should call sendgrid service.", () => {
		const strategy = new SendGridEmailStrategy();
		const email: Email = {
			from: "test@test.com",
			to: ["test@test.com"],
			subject: "Test",
			text: "Test"
		};
		strategy.send(email)
		expect(sendGridService.send).toBeCalledWith(email);
	});
});
