import ResponseError from "../response.error";

describe("ResponseError", () => {
	it("is an error", () => {
		expect(new ResponseError()).toBeInstanceOf(Error);
	});

	describe("default properties.", () => {
		const error = new ResponseError();

		it.each([
			{ prop: "status", value: 500 },
			{ prop: "title", value: "Internal Server Error" },
			{ prop: "detail", value: "" },
			{ prop: "source", value: {} },
			{ prop: "body", value: "" },
			{ prop: "header", value: "" },
			{ prop: "parameter", value: "" },
		])(
			"has property $prop with the default value $value",
			({ prop, value }) => {
				expect(error).toHaveProperty(prop, value);
			}
		);
	});

	describe("status setter", () => {
		const error = new ResponseError();
		error.status = 200;
		it("should set status to the given code.", () => {
			expect(error.status).toBe(200);
		});
		it("should set title to the matching status code title.", () => {
			expect(error.title).toBe("OK");
		});
	});

	describe("source property", () => {
		const error = new ResponseError();
		it.each<{ prop: "header" | "parameter" | "body"; value: any }>([
			{ prop: "header", value: "Content-Type" },
			{ prop: "parameter", value: "p" },
			{ prop: "body", value: "name" },
		])("should set property $prop to the value $value", ({ prop, value }) => {
			error[prop] = value;
			expect(error[prop]).toBe(value);
		});
	});

	describe("toJSON()", () => {
		it("can be converted to JSON", () => {
			const error = new ResponseError();
			error.status = 400;
			error.detail = "Type of the `name` must be a string.";
			error.body = "name";
			const output = JSON.stringify({
				status: error.status,
				title: "Bad Request",
				detail: error.detail,
				source: {
					body: error.body,
				},
			});
			expect(JSON.stringify(error)).toBe(output);
		});
	});
});
