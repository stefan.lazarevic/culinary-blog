import type { Express } from "express";
import AuthenticationRouter from "./authentication.router";
import PostRouter from "./post.router";

export default function bootstrapRoutes(app: Express) {
	app.use(AuthenticationRouter);
	app.use(PostRouter);
}
