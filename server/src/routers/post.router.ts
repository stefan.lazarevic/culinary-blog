import express from "express";
import { param, query } from "express-validator";
import collectValidationErrors from "../middlewares/validate-collect.middleware";

const PostRouter = express.Router();

/**
 * @openapi
 * /posts:
 *   get:
 *     description: Retrieve blog posts.
 *     tags:
 *       - Post
 *     parameters:
 *       - in: query
 *         name: limit
 *         type: number
 *       - in: query
 *         name: offset
 *         type: number
 *       - in: query
 *         name: sort
 *         type: string
 *     responses:
 *       200:
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: array
 *                   items:
 *                     type: string
 *                 links:
 *                   $ref: "#/components/schemas/PaginationLinks"
 *       400:
 *         description: Bad Request
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 errors:
 *                   type: array
 *                   items:
 *                     $ref: "#/components/schemas/ResponseError"
 */
PostRouter.get(
	"/posts",
	query("limit").isInt({ gt: 0 }).default(10).optional().withMessage("value must be an integer greater than 0."),
	query("offset").isInt({ gt: -1 }).default(0).optional().withMessage("value must be an integer greater than 0."),
	query("sort").isString().optional(),
	collectValidationErrors(),
	(req, res) => res.sendStatus(501)
);

/**
 * @openapi
 * /posts/{slug}:
 *   get:
 *     description: Retrieve single resource
 *     tags:
 *       - Post
 *     responses:
 *       200:
 *         description: OK
 *       204:
 *         description: No Content
 */
PostRouter.get("/posts/:slug", param("slug"), (req, res) =>
	res.sendStatus(501)
);

/**
 * @openapi
 * /posts:
 *   post:
 *     description: Store new post.
 *     tags:
 *       - Post
 *     responses:
 *       201:
 *         description: Created
 */
PostRouter.post("/posts", (req, res) =>
	res.sendStatus(501)
);

/**
 * @openapi
 * /posts/{slug}:
 *   put:
 *     description: Replace existing or create a new post record.
 *     tags:
 *       - Post
 *     responses:
 *       200:
 *         description: OK
 *       201:
 *         description: Created
 */
PostRouter.put("/posts/:slug", (req, res) =>
	res.sendStatus(501)
);

/**
 * @openapi
 * /posts/{slug}:
 *   patch:
 *     description: Partially update existing post record.
 *     tags:
 *       - Post
 *     responses:
 *       200:
 *         description: OK
 *       204:
 *         description: No Content
 */
PostRouter.put("/posts/:slug", (req, res) =>
	res.sendStatus(501)
);

/**
 * @openapi
 * /posts/{slug}:
 *   delete:
 *     description: Permanently delete post record.
 *     tags:
 *       - Post
 *     responses:
 *       200:
 *         description: OK
 *       204:
 *         description: No Content
 */
PostRouter.delete("/posts/:slug", (req, res) =>
	res.sendStatus(501)
);

/**
 * @openapi
 * /posts/{slug}/comments:
 *   get:
 *     description: Retrieve single resource
 *     tags:
 *       - Post
 *     responses:
 *       200:
 *         description: OK
 */
PostRouter.get("/posts/:slug/comments", param("slug"), (req, res) =>
	res.sendStatus(501)
);

/**
 * @openapi
 * /posts/{slug}/comments:
 *   post:
 *     description: Store new comment record
 *     tags:
 *       - Post
 *     responses:
 *       201:
 *         description: Created
 */
PostRouter.get("/posts/:slug/comments", param("slug"), (req, res) =>
	res.sendStatus(501)
);

export default PostRouter;
