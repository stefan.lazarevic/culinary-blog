import express, { Response } from "express";
import { body, query } from "express-validator";
import {
	authenticateHandler,
	verificationHandler,
} from "../controllers/authentication.controller";
import InMemoryCache from "../lib/in-memory-cache";
import createJWTMiddleware from "../middlewares/jwt.middleware";
import createRateLimitMiddleware, {
	IRequestRecord,
} from "../middlewares/rate-limit.middleware";
import collectValidationErrors from "../middlewares/validate-collect.middleware";
import getServerOrigin from "../utils/get-server-origin.util";

const AuthenticationRouter = express.Router();
const cache = new InMemoryCache<IRequestRecord>();

/**
 * @openapi
 * /authenticate:
 *   post:
 *     summary: Request magic link for the sign in.
 *     tags:
 *       - Authentication
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               email:
 *                 required: true
 *                 type: string
 *                 format: email
 *     responses:
 *       202:
 *         description: On sent magic link to the provided email address.
 *       400:
 *         description: On the missing email address or incorrect email format.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: "#/components/schemas/ResponseError"
 *       429:
 *         description: On 4th request in one minute.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: "#/components/schemas/ResponseError"
 */
AuthenticationRouter.post(
	"/authenticate",
	[
		body("email").isEmail(),
		collectValidationErrors(),
		// One can request three magic links per minute.
		createRateLimitMiddleware(cache, { max: 3, expiresAt: 60 * 1000 }),
	],
	authenticateHandler
);
/**
 * @openapi
 * /verify:
 *   get:
 *     summary: Verify user
 *     tags:
 *       - Authentication
 *     parameters:
 *       - in: query
 *         name: email
 *         required: true
 *         format: email
 *         description: >
 *           An email address used to receive magic link.
 *       - in: query
 *         name: uuid
 *         required: true
 *         description: >
 *           Universally unique identifier received in matching email.
 *     responses:
 *       200:
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 token:
 *                   type: string
 *       400:
 *         description: Bad Request
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 status:
 *                   type: number
 *                 errors:
 *                   type: array
 *                   items:
 *                     $ref: "#/components/schemas/ResponseError"
 *       500:
 *         description: Internal Server Error
 *         content:
 *           application/json:
 *             schema:
 *               $ref: "#/components/schemas/Error"
 */
AuthenticationRouter.get(
	"/verify",
	query("email").isEmail(),
	query("uuid").isUUID(4),
	collectValidationErrors(),
	verificationHandler
);

AuthenticationRouter.get("/protected", [
	createJWTMiddleware({ issuer: getServerOrigin() }),
	(request: unknown, response: Response) => {
		response.sendStatus(200);
	},
]);

export default AuthenticationRouter;
