import dotenv from "dotenv";
import path from "path";

if (process.env.NODE_ENV === "development") {
	dotenv.config({
		path: path.resolve(__dirname, "../.env.local"),
		override: true,
	});
}

import express from "express";
import getServerPort from "./utils/get-server-port.util";
import bootstrapIncomingMiddlewares, { bootstrapOutgoingMiddlewares } from "./middlewares";
import bootstrapSwagger from "./services/swagger.service";
import bootstrapRoutes from "./routers";
import Logger from "./lib/logger";

(async function bootstrap() {
	const app = express();
	bootstrapIncomingMiddlewares(app);
	bootstrapSwagger(app);
	bootstrapRoutes(app);
	bootstrapOutgoingMiddlewares(app);

	app.listen(getServerPort(), function onApplicationListen() {
		Logger.getInstance().info(`Server is running on the port ${getServerPort()}`);
	});
})();
